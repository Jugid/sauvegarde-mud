from .event import Event2

class MdpOnEvent(Event2):
    NAME = "mdp-on"

    def perform(self):
        if not self.object.has_prop("mdpable"):
            self.fail()
            return self.inform("mdp-on.failed")
        self.inform("mdp-on")
