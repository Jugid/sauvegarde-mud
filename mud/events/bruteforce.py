from .event import Event2
import time

class BruteEvent(Event2):
    NAME = "bruteforce"
    def perform(self):
        if self.object.has_prop("bruteforce"):
            self.buffer_clear()
            self.buffer_append("Chargement de la base de données...")
            self.actor.send_result(self.buffer_get())
            time.sleep(1)
            self.buffer_clear()
            self.buffer_append("Verification des solutions possibles...")
            self.actor.send_result(self.buffer_get())
            time.sleep(1)
            self.buffer_clear()
            self.buffer_append("Chargement des solutions possibles...")
            self.actor.send_result(self.buffer_get())
            time.sleep(3)
            self.buffer_clear()
            self.buffer_append("Fait !")
            self.actor.send_result(self.buffer_get())
            time.sleep(3)
            self.buffer_clear()
            self.buffer_append("ERROR : IMPOSSIBLE DE TROUVER UN MOT DE PASSE")
            self.actor.send_result(self.buffer_get())
            time.sleep(1)
            self.buffer_clear()
            self.buffer_append("Elargissement des possibilités")
            self.actor.send_result(self.buffer_get())
            time.sleep(0.5)
            self.buffer_clear()
            self.buffer_append("ERROR : DATA_BASE_FAILED : LIGNE DECALEE")
            self.actor.send_result(self.buffer_get())
            time.sleep(2)
            self.buffer_clear()
            self.buffer_append("Mot de passe possible : qsdfghjklm")
            self.actor.send_result(self.buffer_get())
