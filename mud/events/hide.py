# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2, Event3

class HideEvent(Event2):
    NAME = "hidden"

    def perform(self):
        self.inform("hidden")
