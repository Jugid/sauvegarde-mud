from .event import Event1

class MapEvent(Event1):
    def perform(self):
        self.buffer_clear()
        self.buffer_append("Vous vous mettez à danser de façon frénétique!")
        self.buffer_append("<img src='https://media.giphy.com/media/elbwkxVuAfGiQ/giphy.gif'/>")
        self.actor.send_result(self.buffer_get())
